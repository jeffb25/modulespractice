package com.example.feature_drinks.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.feature_drinks.view.CategoryDrinksState
import kotlinx.coroutines.launch

class CategoryDrinksViewModel(category: String) : ViewModel() {
    private val repo = BottomsUpRepo
    private val _state = MutableLiveData(CategoryDrinksState(isLoading = true))
    val state: LiveData<CategoryDrinksState> get() = _state

    init {
        viewModelScope.launch {
            val categoryDrinks = repo.getCategoryDrinks(category)
            _state.value = CategoryDrinksState(categoryDrinks = categoryDrinks)
        }
    }


}