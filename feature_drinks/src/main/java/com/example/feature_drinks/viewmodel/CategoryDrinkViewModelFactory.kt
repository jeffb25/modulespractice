package com.example.feature_drinks.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CategoryDrinkViewModelFactory(
    private val category: String
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoryDrinksViewModel(category) as T
    }

}