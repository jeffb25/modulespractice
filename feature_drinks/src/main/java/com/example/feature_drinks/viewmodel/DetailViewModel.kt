package com.example.feature_drinks.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.feature_drinks.view.DetailState
import kotlinx.coroutines.launch

class DetailViewModel(details: Int) : ViewModel() {

    private val repo = BottomsUpRepo
    private val _state = MutableLiveData(DetailState(isLoading = true))
    val state: LiveData<DetailState> get() = _state

    init {
        viewModelScope.launch {
            val details = repo.getDetails(details)
            _state.value = DetailState(details = details)
        }
    }
}