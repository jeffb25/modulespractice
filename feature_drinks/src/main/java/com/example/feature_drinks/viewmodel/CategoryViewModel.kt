package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.feature_drinks.view.CategoryState
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel(){
    private val repo by lazy { BottomsUpRepo }
    private val _state = MutableLiveData(CategoryState(isLoading = true))
    val state: LiveData<CategoryState> get() = _state


    init {
        viewModelScope.launch {
          val categoryDTO =  repo.getCategories()
            _state.value = CategoryState(categories = categoryDTO)
            Log.d("CategoryViewModel", categoryDTO.toString())
        }
    }

}