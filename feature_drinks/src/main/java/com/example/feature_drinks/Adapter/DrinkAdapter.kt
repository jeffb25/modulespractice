package com.example.feature_drinks.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView

import com.example.feature_drinks.model.response.CatergoryDrinksDTO

import com.example.feature_drinks.databinding.ItemCategoryDrinksBinding
import com.example.feature_drinks.view.DrinksFragmentDirections
import com.squareup.picasso.Picasso

class DrinkAdapter(
    private val drinks: List<CatergoryDrinksDTO.Drink>,
    private val onDrinkClicked: (CatergoryDrinksDTO.Drink) -> Unit
) : RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = DrinkViewHolder.getInstance(parent, onDrinkClicked)

    override fun onBindViewHolder(
        holder: DrinkViewHolder, position: Int
    ) {
        val drink = drinks[position]
        holder.bindDrink(drink)
    }

    class DrinkViewHolder(
        private val binding: ItemCategoryDrinksBinding,
        private val onDrinkClicked: (CatergoryDrinksDTO.Drink) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindDrink(drink: CatergoryDrinksDTO.Drink) = with(
            binding
        ) {
            binding.tvCategoryDrinks.text = drink.strDrink
            binding.tvCategoryDrinks.setOnClickListener {
                onDrinkClicked.invoke(drink)
                it.findNavController().navigate(DrinksFragmentDirections.actionDrinksFragmentToDetailFragment(drink.idDrink.toInt()))
            }
            Picasso.get().load(drink.strDrinkThumb).into(binding.tvCategoryDrinks3)
        }

        companion object {
            fun getInstance(
                parent: ViewGroup,
                onDrinkClicked: (CatergoryDrinksDTO.Drink) -> Unit
            ) = ItemCategoryDrinksBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).let { binding -> DrinkViewHolder(binding,onDrinkClicked) }
        }

    }

    override fun getItemCount(): Int = drinks.size

}