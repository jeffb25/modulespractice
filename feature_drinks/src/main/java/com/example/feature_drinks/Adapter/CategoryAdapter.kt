package com.example.feature_drinks.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView

import com.example.feature_drinks.model.response.CategoryDTO
import com.example.feature_drinks.view.CategoryFragmentDirections
import com.example.feature_drinks.databinding.ItemCategoriesBinding

class CategoryAdapter (
    private val categoryClicked: (category: CategoryDTO.CategoryItem) -> Unit) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var categories = mutableListOf<CategoryDTO.CategoryItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int
    ) = CategoryViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener { categoryClicked(categories[adapterPosition]) }
    }


    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.bindCategory(category)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

     fun addCategories(categories: List<CategoryDTO.CategoryItem>){
        this.categories = categories.toMutableList()
        notifyDataSetChanged()
    }

    fun loadCategories(categories: List<CategoryDTO.CategoryItem> ){
        this.categories.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0,oldSize)
            addAll(categories)
            notifyItemRangeInserted(0,categories.size)
        }

    }


    class CategoryViewHolder(
        private val binding: ItemCategoriesBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindCategory(category: CategoryDTO.CategoryItem) = with(binding){

            tvCategory.text = category.strCategory
            binding.tvCategory.setOnClickListener {
                it.findNavController().navigate(CategoryFragmentDirections.actionCategoryFragmentToDrinksFragment(category.strCategory))
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoriesBinding
                .inflate(LayoutInflater.from(parent.context),
                parent,
                false).let { CategoryViewHolder(it) }
        }





    }

}