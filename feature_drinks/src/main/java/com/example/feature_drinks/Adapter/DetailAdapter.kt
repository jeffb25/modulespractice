package com.example.feature_drinks.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.example.feature_drinks.model.response.DrinkDetailsDTO
import com.example.feature_drinks.databinding.ItemDetailBinding
import com.squareup.picasso.Picasso

class DetailAdapter(
    private val details: List<DrinkDetailsDTO.Drink>,
    private val onDetails: (DrinkDetailsDTO.Drink) -> Unit
) : RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
            viewType: Int) = DetailViewHolder.getInstance(parent, onDetails)


    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        val detail = details[position]
        holder.bindDetail(detail)
    }

    override fun getItemCount(): Int = details.size


    class DetailViewHolder(
        private val binding: ItemDetailBinding,
        private val onDetails: (DrinkDetailsDTO.Drink) -> Unit
        ) : RecyclerView.ViewHolder(binding.root) {
        fun bindDetail(detail: DrinkDetailsDTO.Drink) = with(
            binding
        ) {
            onDetails.invoke(detail)
            binding.tvDrinkName.text = detail.strDrink
            Picasso.get().load(detail.strDrinkThumb).into(binding.ivDrink)
            binding.tvDrinkIngredient.text = detail.strIngredient1
            binding.tvDrinkIngredient2.text = detail.strIngredient2
            binding.tvDrinkIngredient3.text = detail.strIngredient3
            binding.tvDrinkIngredient4.text = detail.strIngredient4
            binding.tvDrinkInstructions.text = detail.strInstructions

        }


        companion object {
            fun getInstance(
                parent: ViewGroup,
                onDetails: (DrinkDetailsDTO.Drink) -> Unit
            ) = ItemDetailBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).let { binding -> DetailViewHolder(binding, onDetails) }
        }


    }



}