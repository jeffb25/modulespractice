package com.example.feature_drinks.view

import com.example.feature_drinks.model.response.DrinkDetailsDTO

data class DetailState(
    val isLoading: Boolean = false,
    val details: List<DrinkDetailsDTO.Drink> = emptyList()
)
