package com.example.feature_drinks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.feature_drinks.Adapter.CategoryAdapter
import com.example.feature_drinks.model.response.CategoryDTO
import com.example.bottomsup.viewmodel.CategoryViewModel
import com.example.feature_drinks.databinding.FragmentCategoryBinding

class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
    private val categoryAdapter by lazy { CategoryAdapter(::categoryClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentCategoryBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvList.adapter = categoryAdapter
        categoryViewModel.state.observe(viewLifecycleOwner){state ->
            binding.loader.isVisible = state.isLoading
            categoryAdapter.loadCategories(state.categories)
        }



    }
    fun categoryClicked(category: CategoryDTO.CategoryItem){
        Toast.makeText(context, category.toString(), Toast.LENGTH_SHORT).show()

    }

}