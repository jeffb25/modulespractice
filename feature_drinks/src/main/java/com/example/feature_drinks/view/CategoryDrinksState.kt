package com.example.feature_drinks.view

import com.example.feature_drinks.model.response.CatergoryDrinksDTO

data class CategoryDrinksState(
    val isLoading: Boolean = false,
    val categoryDrinks: List<CatergoryDrinksDTO.Drink> = emptyList()
)
