package com.example.feature_drinks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.feature_drinks.Adapter.DrinkAdapter

import com.example.feature_drinks.viewmodel.CategoryDrinkViewModelFactory
import com.example.feature_drinks.viewmodel.CategoryDrinksViewModel
import com.example.feature_drinks.databinding.FragmentDrinksBinding


class DrinksFragment : Fragment() {
    private var _binding: FragmentDrinksBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinksFragmentArgs>()
    private val categoryDrinkViewModel by viewModels<CategoryDrinksViewModel> {
        CategoryDrinkViewModelFactory(args.category)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinksBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvDrinks.text = args.category
        categoryDrinkViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.loader.isVisible = state.isLoading
            if (state.categoryDrinks.isNotEmpty()) {
                binding.rvList2.adapter = DrinkAdapter(state.categoryDrinks) { drink ->
                    Toast.makeText(context, drink.strDrink, Toast.LENGTH_SHORT).show()
                }

            }
        }


    }
}