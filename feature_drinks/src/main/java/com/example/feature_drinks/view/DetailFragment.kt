package com.example.feature_drinks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.feature_drinks.Adapter.DetailAdapter

import com.example.feature_drinks.viewmodel.DetailViewModel
import com.example.feature_drinks.viewmodel.DetailViewModelFactory
import com.example.feature_drinks.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DetailFragmentArgs>()
    private val detailViewModel by viewModels<DetailViewModel> {
        DetailViewModelFactory(args.detail)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentDetailBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvDetails.text = args.detail.toString()
        detailViewModel.state.observe(viewLifecycleOwner){ state ->
            binding.loader.isVisible = state.isLoading
            if(state.details.isNotEmpty()) {
                binding.details.adapter = DetailAdapter(state.details){ detail ->
                    Toast.makeText(context, detail.strDrink, Toast.LENGTH_SHORT).show().toString()
                }
                }
        }


    }
}