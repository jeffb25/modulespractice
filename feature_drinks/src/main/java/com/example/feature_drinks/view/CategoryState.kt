package com.example.feature_drinks.view

import com.example.feature_drinks.model.response.CategoryDTO

data class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<CategoryDTO.CategoryItem> = emptyList()


)
