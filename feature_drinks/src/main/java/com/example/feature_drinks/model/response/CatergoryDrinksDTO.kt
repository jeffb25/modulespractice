package com.example.feature_drinks.model.response


import com.google.gson.annotations.SerializedName

data class CatergoryDrinksDTO(
    @SerializedName("drinks")
    val drinks: List<Drink>
){
    data class Drink(
        val idDrink: String,
        val strDrink: String,
        val strDrinkThumb: String
    )
}