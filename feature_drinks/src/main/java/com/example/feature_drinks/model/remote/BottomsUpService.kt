package com.example.feature_drinks.model.remote

import com.example.feature_drinks.model.response.CategoryDTO
import com.example.feature_drinks.model.response.CatergoryDrinksDTO
import com.example.feature_drinks.model.response.DrinkDetailsDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface BottomsUpService {

    companion object{
        private const val BASE_URL = "https://www.thecocktaildb.com"

        fun getInstance() : BottomsUpService {
            val retrofit : Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }

    @GET("/api/json/v1/1/list.php")
    suspend fun getCategories(@Query("c") type: String = "list"): CategoryDTO
    @GET("/api/json/v1/1/filter.php")
    suspend fun getCatergoryDrinks(@Query("c") category: String): CatergoryDrinksDTO
    @GET("/api/json/v1/1/lookup.php")
    suspend fun getDrinkDetails(@Query("i") detailId: Int): DrinkDetailsDTO
}