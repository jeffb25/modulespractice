package com.example.bottomsup.model

import com.example.feature_drinks.model.remote.BottomsUpService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {

    private val bottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories() = withContext(Dispatchers.IO){
        bottomsUpService.getCategories().categoryItems
    }
    suspend fun getCategoryDrinks(category: String) = withContext(Dispatchers.IO){
        bottomsUpService.getCatergoryDrinks(category).drinks
    }
    suspend fun getDetails(details: Int) = withContext(Dispatchers.IO){
        bottomsUpService.getDrinkDetails(details).drinks
    }
}