package com.rave.feature_shibe.viewmodel

import androidx.lifecycle.*
import com.rave.feature_shibe.model.ShibeRepo
import com.rave.feature_shibe.model.local.entity.Shibe

class ShibeViewModel(private val repo: ShibeRepo) : ViewModel() {

    val state: LiveData<ShibeState> = liveData {
        emit(ShibeState(isLoading = true))
        val shibes = repo.getShibes()
        emit(ShibeState(shibes = shibes))
    }

    data class ShibeState(
        val isLoading: Boolean = false,
        val shibes: List<Shibe> = emptyList()
    )

    class ShibeViewModelFactory(
        private val shibeRepo: ShibeRepo
    ) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ShibeViewModel(shibeRepo) as T
        }
    }
}