package com.rave.feature_shibe.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {

    companion object {
        private const val BASE_URL = "https://shibe.online"

        fun getInstance() = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create<ShibeService>()
    }

    @GET("/api/shibes")
    suspend fun getShibes(@Query("count") count: Int = 100): List<String>
}