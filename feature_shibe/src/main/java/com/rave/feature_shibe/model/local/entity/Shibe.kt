package com.rave.feature_shibe.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Shibe(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val url: String
)