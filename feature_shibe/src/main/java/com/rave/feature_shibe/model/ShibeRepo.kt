package com.rave.feature_shibe.model

import android.content.Context
import android.util.Log
import com.rave.feature_shibe.model.local.ShibeDatabase
import com.rave.feature_shibe.model.local.entity.Shibe
import com.rave.feature_shibe.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val TAG = "ShibeRepo"
class ShibeRepo(context: Context) {

    private val shibeService = ShibeService.getInstance()
    private val shibeDao = ShibeDatabase.getInstance(context).shibeDao()

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()

        return@withContext cachedShibes.ifEmpty {
            val shibeUrls: List<String> = shibeService.getShibes()
            Log.d(TAG, "shibeUrls size is ${shibeUrls.size}")
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            Log.d(TAG, "shibes size is ${shibes.size}")
            shibeDao.insert(shibes)
            Log.d(TAG, "shibes size is ${shibeDao.getAll().size}")
            return@ifEmpty shibes
        }
    }
}