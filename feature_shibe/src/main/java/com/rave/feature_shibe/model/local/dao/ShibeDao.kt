package com.rave.feature_shibe.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rave.feature_shibe.model.local.entity.Shibe

@Dao
interface ShibeDao {

    @Query("SELECT * FROM shibe")
    suspend fun getAll(): List<Shibe>

    @Insert
    suspend fun insert(shibe: List<Shibe>)

}