package com.rave.feature_shibe.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rave.feature_shibe.model.local.dao.ShibeDao
import com.rave.feature_shibe.model.local.entity.Shibe

@Database(entities = [Shibe::class], version = 1)
abstract class ShibeDatabase : RoomDatabase() {

    abstract fun shibeDao(): ShibeDao

    companion object {

        private const val DATABASE_NAME = "shibe.db"

        // For Singleton instantiation
        @Volatile
        private var instance: ShibeDatabase? = null

        fun getInstance(context: Context): ShibeDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        // Create and pre-populate the database. See this article for more details:
        // https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1#4785
        private fun buildDatabase(context: Context): ShibeDatabase {
            return Room.databaseBuilder(context, ShibeDatabase::class.java, DATABASE_NAME).build()
        }
    }
} 